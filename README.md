# Kompilator za fragment jezika Haskell
## Projekat za kurs "Konstrukcija kompilatora"

Generisanje LLVM IR-a za fragment koda u Haskellu.

### Primer korišćenja

```
	make
	./hask < test
	clang -o hask_run main.c test.ll
	./hask_run
```


