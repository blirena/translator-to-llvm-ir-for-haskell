%option noyywrap
%option nounput
%option noinput

%{
#include <iostream>
  #include <cstdlib>

  using namespace std;

  #include "ast.hpp"
  #include "parser.tab.hpp"
%}

%%
"otherwise"              return otherwise_token;
"if"                     return if_token;
"then"                   return then_token;
"else"                   return else_token;
"let"                    return let_token;
"=="                     return eq_token;
"/="                     return neq_token;
">="                     return ge_token;
"<="                     return le_token;

[0-9]+(\.[0-9]*)?        { yylval.d = atof(yytext);
                           return num_token;     
                         }
[a-zA-Z_][a-zA-Z0-9_]*   { yylval.s = new string(yytext);
                           return id_token;
                         }
[-+*/()|=<>_:\n,\[\]]    return *yytext;
[ \t]                    {}
.                        { cerr << "leksicka greska: nepoznat karakter ";
                           cerr << *yytext << endl;
			   exit(EXIT_FAILURE);
                         }  
%%
