; ModuleID = 'my_module'

define double @c() {
entry:
  ret double 6.000000e+00
}

define double @inc(double %x) {
entry:
  %addtmp = fadd double %x, 1.000000e+00
  ret double %addtmp
}

define double @"0"() {
entry:
  %calltmp = call double @inc(double 2.000000e+00)
  ret double %calltmp
}

define double @dec(double %x) {
entry:
  %addtmp = fadd double %x, -1.000000e+00
  ret double %addtmp
}

define double @addsqr(double %x, double %y) {
entry:
  %addtmp = fmul double %x, %x
  %addtmp1 = fmul double %y, %y
  %addtmp2 = fadd double %addtmp, %addtmp1
  ret double %addtmp2
}

define double @"1"() {
entry:
  %calltmp = call double @addsqr(double 5.000000e+00, double 6.000000e+00)
  ret double %calltmp
}

define double @sum(double %n) {
entry:
  %eqtmp = fcmp ueq double %n, 0.000000e+00
  br i1 %eqtmp, label %ifcont, label %else

else:                                             ; preds = %entry
  %calltmp = call double @dec(double %n)
  %calltmp1 = call double @sum(double %calltmp)
  %addtmp = fadd double %calltmp1, %n
  br label %ifcont

ifcont:                                           ; preds = %entry, %else
  %iftmp = phi double [ %addtmp, %else ], [ 0.000000e+00, %entry ]
  ret double %iftmp
}

define double @max(double %a, double %b) {
entry:
  %gttmp = fcmp ugt double %a, %b
  %gttmp1 = fcmp ugt double %b, %a
  %b.a = select i1 %gttmp1, double %b, double %a
  %iftmp7 = select i1 %gttmp, double %a, double %b.a
  ret double %iftmp7
}

define double @"2"() {
entry:
  %calltmp = call double @max(double 1.000000e+01, double 1.500000e+01)
  ret double %calltmp
}
