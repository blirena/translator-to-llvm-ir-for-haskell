#include "ast.hpp"

#include <iostream>
#include <cctype>

LLVMContext the_context;
IRBuilder<> builder(the_context);
Module *the_module;
llvm::legacy::FunctionPassManager *the_FPM;
extern map<string, Value*> named_values;

ExprAST::~ExprAST() {
  
}

Value *NumberExprAST::codegen() const {
    return ConstantFP::get(the_context, APFloat(_val));
}

Value *VariableExprAST::codegen() const {
    map<string, Value*>::iterator tmp = named_values.find(_name);
    if (tmp == named_values.end()) {
        cerr << "Variable " + _name + " not defined" << endl;
        return NULL;
    }
    return tmp->second;
}

string VariableExprAST::getName() const {
    return _name;
}

BinaryExprAST::~BinaryExprAST() {
    delete _left;
    delete _right;
}

CondExprAST::~CondExprAST() {
  
}

Value *AddExprAST::codegen() const {
    Value *l = _left->codegen();
    Value *r = _right->codegen();
    if (l == NULL || r == NULL)
        return NULL;
    return builder.CreateFAdd(l, r, "addtmp");
}

Value *SubExprAST::codegen() const {
    Value *l = _left->codegen();
    Value *r = _right->codegen();
    if (l == NULL || r == NULL)
        return NULL;
    return builder.CreateFSub(l, r, "addtmp");
}

Value *MulExprAST::codegen() const {
    Value *l = _left->codegen();
    Value *r = _right->codegen();
    if (l == NULL || r == NULL)
        return NULL;
    return builder.CreateFMul(l, r, "addtmp");
}

Value *DivExprAST::codegen() const {
    Value *l = _left->codegen();
    Value *r = _right->codegen();
    if (l == NULL || r == NULL)
        return NULL;
    return builder.CreateFDiv(l, r, "addtmp");
}

Value *EqExprAST::codegen() const {
    Value *l = _left->codegen();
    Value *r = _right->codegen();
    if (l == NULL || r == NULL)
        return NULL;
    l = builder.CreateFCmpUEQ(l, r, "eqtmp");
    return builder.CreateUIToFP(l, Type::getDoubleTy(the_context),
                                "booltmp");
}

Value *LETExprAST::codegen() const {
    Value *l = _left->codegen();
    Value *r = _right->codegen();
    if (l == NULL || r == NULL)
        return NULL;
    l = builder.CreateFCmpULE(l, r, "lettmp");
    return builder.CreateUIToFP(l, Type::getDoubleTy(the_context),
                                "booltmp");
}

Value *GETExprAST::codegen() const {
    Value *l = _left->codegen();
    Value *r = _right->codegen();
    if (l == NULL || r == NULL)
        return NULL;
    l = builder.CreateFCmpUGE(l, r, "gettmp");
    return builder.CreateUIToFP(l, Type::getDoubleTy(the_context),
                                "booltmp");
}
Value *NEExprAST::codegen() const {
    Value *l = _left->codegen();
    Value *r = _right->codegen();
    if (l == NULL || r == NULL)
        return NULL;
    l = builder.CreateFCmpUNE(l, r, "netmp");
    return builder.CreateUIToFP(l, Type::getDoubleTy(the_context),
                                "booltmp");
}
Value *LTExprAST::codegen() const {
    Value *l = _left->codegen();
    Value *r = _right->codegen();
    if (l == NULL || r == NULL)
        return NULL;
    l = builder.CreateFCmpULT(l, r, "lttmp");
    return builder.CreateUIToFP(l, Type::getDoubleTy(the_context),
                                 "booltmp");
}
Value *GTExprAST::codegen() const {
    Value *l = _left->codegen();
    Value *r = _right->codegen();
    if (l == NULL || r == NULL)
        return NULL;
    l = builder.CreateFCmpUGT(l, r, "gttmp");
    return builder.CreateUIToFP(l, Type::getDoubleTy(the_context),
                                "booltmp");
}

IfExprAST::~IfExprAST() {
    delete _cond;
    delete _then;
    delete _else;
}

Value *IfExprAST::codegen() const {
    Value *cond_v = _cond->codegen();
    if (cond_v == NULL)
        return NULL;
    cond_v = builder.CreateFCmpONE(cond_v,
                                   ConstantFP::get(the_context, APFloat(0.0)), "ifcond");
    Function *f = builder.GetInsertBlock()->getParent();

    // create and insert then block at the end of the function
    BasicBlock *then_bb = BasicBlock::Create(the_context, "then", f);
    // create else and merge block (insertion later)
    BasicBlock *else_bb = BasicBlock::Create(the_context, "else");
    BasicBlock *merge_bb = BasicBlock::Create(the_context, "ifcont");

    // create conditional break depending on cond_v value
    builder.CreateCondBr(cond_v, then_bb, else_bb);

    // filling the then block
    builder.SetInsertPoint(then_bb);
    Value *then_v = _then->codegen();
    if (then_v == NULL)
        return NULL;
    // unconditional break to merge block
    builder.CreateBr(merge_bb);
    // in case it changed during codegen()
    then_bb = builder.GetInsertBlock();

    f->getBasicBlockList().push_back(else_bb);
    builder.SetInsertPoint(else_bb);
    Value *else_v = NULL;
    if (_else != NULL) {
        else_v = _else->codegen();
        if (else_v == NULL)
            return NULL;
    }
    builder.CreateBr(merge_bb);
    else_bb = builder.GetInsertBlock();

    f->getBasicBlockList().push_back(merge_bb);
    builder.SetInsertPoint(merge_bb);
    // takes value depending on what preceded it
    PHINode *phi = builder.CreatePHI(Type::getDoubleTy(the_context),
                                     2, "iftmp");
    phi->addIncoming(then_v, then_bb);
    phi->addIncoming(else_v, else_bb);

    return phi;
}

SwitchExprAST::~SwitchExprAST() {
    for (auto &a : _conds_exprs) {
        delete a.first;
        delete a.second;
    }
}

Value *SwitchExprAST::codegen() const {
    int n = _conds_exprs.size();
    if (n > 1) {
        vector<IfExprAST*> cases(n, NULL);
        if (_conds_exprs[n-1].first == NULL) {
            cases[n-2] = new IfExprAST(_conds_exprs[n-2].first,
                                       _conds_exprs[n-2].second,
                                       _conds_exprs[n-1].second);
            n -= 2;
        }
        else {
            cases[n-1] = new IfExprAST(_conds_exprs[n-1].first,
                                       _conds_exprs[n-1].second, NULL);
            n--;
        }
        for (int i = n - 1; i >= 0; --i) {
            cases[i] = new IfExprAST(_conds_exprs[i].first,
                                     _conds_exprs[i].second, cases[i+1]);
        }
        // cases[n] is generated from cases[n-1]
        for (int i = 0; i < n - 1; ++i)
            cases[i]->codegen();
        return cases[n-1]->codegen();
    }
    else {
        IfExprAST *tmp = new IfExprAST(_conds_exprs[0].first,
                                       _conds_exprs[0].second, NULL);
        return tmp->codegen();
    }
}

CallExprAST::~CallExprAST() {
    for (unsigned i = 0; i < _params.size(); ++i)
        delete _params[i];
}

string CallExprAST::getName() const {
    return _name;
}

Value *CallExprAST::codegen() const {
    Function *callee = the_module->getFunction(_name);
    if (!callee) {
        cerr << "Function " << _name << " is not defined" << endl;
        return NULL;
    }

    vector<Value*> params_v;
    if (callee->arg_size() != _params.size()) {
        cout << "Function " << _name << " has " << callee->arg_size() << " argument(s)" << endl;
        return NULL;
    }
    else {
        for (unsigned i = 0; i < _params.size(); ++i) {
            params_v.push_back(_params[i]->codegen());
            // in case last codegen didn't pass, return null
            if (!params_v.back())
                return NULL;
        }
    }
    return builder.CreateCall(callee, params_v, "calltmp");
}

FunctionAST::~FunctionAST() {
    delete _body;
}

vector<string> FunctionAST::getArgs() const {
    return _args;
}

string FunctionAST::getName() const {
    return _name;
}

Function *FunctionAST::codegen() const {
    Function *the_function = the_module->getFunction(_name);

    if (the_function && !the_function->empty()) {
        // deleting the body to predifine the function
        the_function->eraseFromParent();
    }

    vector<Type*> arguments(_args.size(), Type::getDoubleTy(the_context));
    FunctionType *ft = FunctionType::get(Type::getDoubleTy(the_context),
                                         arguments, false);
    the_function = Function::Create(ft, Function::ExternalLinkage,
                                    _name, the_module);
    unsigned i = 0;
    for (auto &a : the_function->args()) {
        a.setName(_args[i++]);
        // save arguments in symbol table
        named_values[a.getName()] = &a;
    }

    // creating new basic block for insertion
    BasicBlock *bb = BasicBlock::Create(the_context, "entry", the_function);
    builder.SetInsertPoint(bb);

    // generate code for the body
    if (Value *retval = _body->codegen()) {
        builder.CreateRet(retval);
        verifyFunction(*the_function);
        the_FPM->run(*the_function);
        return the_function;
    }

    // otherwise
    the_function->eraseFromParent();
    return NULL;
}

void init_module_and_fpm(void) {
    the_module = new Module("my_module", the_context);

    // Create a new pass manager attached to it.
    the_FPM = new llvm::legacy::FunctionPassManager(the_module);

    // Do simple "peephole" optimizations and bit-twiddling optzns.
    the_FPM->add(createInstructionCombiningPass());
    // Eliminate common subexpressions.
    the_FPM->add(createGVNPass());
    // Simplify the control flow graph (deleting unreachable blocks, merging
    // basic blocks, etc)
    the_FPM->add(createCFGSimplificationPass());

    the_FPM->doInitialization();
}
