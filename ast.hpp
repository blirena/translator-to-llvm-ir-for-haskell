#ifndef __AST_HPP__
#define __AST_HPP__ 1

#include <string>
#include <vector>
using namespace std;

#include "llvm/IR/Module.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/IR/Verifier.h"
#include "llvm/IR/LegacyPassManager.h"
#include "llvm-c/Core.h"
#include "llvm/Transforms/Scalar.h"

using namespace llvm;
using namespace llvm::legacy;

class ExprAST {
public:
    virtual ~ExprAST();
    virtual Value *codegen() const = 0;
};

class NumberExprAST : public ExprAST {
public:
    NumberExprAST( double v )
        :_val(v)
    {}
    Value *codegen() const;
private:
    double _val;
};

class VariableExprAST : public ExprAST {
public:
    VariableExprAST( string name )
        :_name(name)
    {}
    Value *codegen() const;
    string getName() const;
private:
    string _name;
};

class BinaryExprAST : public ExprAST {
public:
    BinaryExprAST( char op, ExprAST *left, ExprAST *right )
        :_left(left), _right(right), _op(op)
    {}
    ~BinaryExprAST();
protected:
    ExprAST *_left, *_right;
private:
    char _op;
    BinaryExprAST( const BinaryExprAST &bin );
    BinaryExprAST &operator=( const BinaryExprAST &bin);
};

class CondExprAST : public BinaryExprAST {
public:
    CondExprAST(char op, ExprAST *left, ExprAST *right)
        :BinaryExprAST(op, left, right)
    {}
    ~CondExprAST();
};

class AddExprAST : public BinaryExprAST {
public:
    AddExprAST( ExprAST *left, ExprAST *right )
        :BinaryExprAST( '+', left, right)
    {}
    Value *codegen() const;
};

class SubExprAST : public BinaryExprAST {
public:
    SubExprAST( ExprAST *left, ExprAST *right )
        :BinaryExprAST( '-', left, right)
    {}
    Value *codegen() const;
};

class MulExprAST : public BinaryExprAST {
public:
    MulExprAST( ExprAST *left, ExprAST *right )
        :BinaryExprAST( '*', left, right)
    {}
    Value *codegen() const;
};

class DivExprAST : public BinaryExprAST {
public:
    DivExprAST( ExprAST *left, ExprAST *right )
        :BinaryExprAST( '/', left, right)
    {}
    Value *codegen() const;
};

class EqExprAST : public CondExprAST {
public:
    EqExprAST( ExprAST *left, ExprAST *right )
        :CondExprAST( '=', left, right)
    {}
    Value *codegen() const;
};

class LETExprAST : public CondExprAST {
public:
    LETExprAST( ExprAST *left, ExprAST *right )
        :CondExprAST( 'l', left, right)
    {}
    Value *codegen() const;
};

class GETExprAST : public CondExprAST {
public:
    GETExprAST( ExprAST *left, ExprAST *right )
        :CondExprAST( 'g', left, right)
    {}
    Value *codegen() const;
};

class NEExprAST : public CondExprAST {
public:
    NEExprAST( ExprAST *left, ExprAST *right )
        :CondExprAST( 'n', left, right)
    {}
    Value *codegen() const;
};

class LTExprAST : public CondExprAST {
public:
    LTExprAST( ExprAST *left, ExprAST *right )
        :CondExprAST( '<', left, right)
    {}
    Value *codegen() const;
};

class GTExprAST : public CondExprAST {
public:
    GTExprAST( ExprAST *left, ExprAST *right )
        :CondExprAST( '>', left, right)
    {}
    Value *codegen() const;
};

class IfExprAST : public ExprAST {
public:
    IfExprAST( CondExprAST* a, ExprAST* b, ExprAST* c )
        :_cond(a), _then(b), _else(c)
    {}
    Value *codegen() const;
    ~IfExprAST();
private:
    CondExprAST *_cond;
    ExprAST *_then, *_else;
    IfExprAST( const IfExprAST &i );
    IfExprAST &operator=( const IfExprAST &i );
};

class SwitchExprAST : public ExprAST {
public:
    SwitchExprAST(vector<pair<CondExprAST*, ExprAST*>> conds_exprs)
        :_conds_exprs(conds_exprs)
    {}
    Value *codegen() const;
    ~SwitchExprAST();
private:
    vector<pair<CondExprAST*, ExprAST*>> _conds_exprs;
    SwitchExprAST( const SwitchExprAST &s );
    SwitchExprAST &operator=( const SwitchExprAST &s );
};

class CallExprAST : public ExprAST {
public:
    CallExprAST( string name, vector<ExprAST*> params )
        :_name(name), _params(params)
    {}
    Value *codegen() const;
    string getName() const;
    ~CallExprAST();
private:
    string _name;
    vector<ExprAST*> _params;
    CallExprAST( const CallExprAST &c );
    CallExprAST &operator=( const CallExprAST &c );
};

class FunctionAST {
public:
    FunctionAST( string s, vector<string> args, ExprAST *body )
        :_name(s), _args(args), _body(body)
    {}
    ~FunctionAST();
    Function *codegen() const;
    vector<string> getArgs() const;
    string getName() const;
private:
    FunctionAST( const FunctionAST &f );
    FunctionAST &operator=( const FunctionAST &f );
    string _name;
    vector<string> _args;
    ExprAST *_body;
};


void init_module_and_fpm(void);

#endif // __AST_HPP__
