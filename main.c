#include <stdio.h>

double max(double, double);
double inc(double);
double addsqr(double, double);
double sum(double);
double c();

int main() {
    printf("max: %g\n", max(10, 15));
    printf("inc: %g\n", inc(3.5));
    printf("addsqr: %g\n", addsqr(4, 3));
    printf("sum: %g\n", sum(10));
    printf("c: %g\n", c());
    return 0;
}
